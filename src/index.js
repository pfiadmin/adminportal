function loaduserdata(){

  var users = [];

  var i = 0

  $.get("http://localhost:4000/user/getallusers", function( data ) {
    for (var item in data) {
      users.push({
        "userID" : data[item]['userID'],
        "fName" : data[item]['name']['fName'],
        "lName" : data[item]['name']['lName'],
        "email" : data[item]['email'],
        "Password" : data[item]['saltPassword'],
        "mobile" : data[item]['mobile'],
        "firmName" : data[item]['firmName'],
        "dateofBirth" : data[item]['dateofBirth'],
        "address1" : data[item]['address1'],
        "address2" : data[item]['address2'],
        "address3" : data[item]['address3'],
        "pincode" : data[item]['pincode'],
        "stateID" : data[item]['stateID'],
        "gstNo" : data[item]['gstNo'],
        "userType" : data[item]['userType'],
        "distributorID" : data[item]['distributorID'],
        "createdAt" : data[item]['createdAt']
      });


      if (i == data.length -1){

        $("#jsGrid_user").jsGrid({
            width: "100%",
            height: "400px",
            data: users,
            sorting: true,
            paging: true,

            fields: [
                { name: "userID", type: "text",validate: "required" , align: "center" },
                { name: "fName", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "lName", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "email", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "Password", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "mobile", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "firmName", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "dateofBirth", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "address1", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "address2", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "address3", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "pincode", type: "text", validate: "required" , align: "center" },
                { name: "stateID", type: "text", validate: "required" , align: "center" },
                { name: "gstNo", type: "text", validate: "required" , align: "center" },
                { name: "userType", type: "text", validate: "required" , align: "center" },
                { name: "distributorID", type: "text", validate: "required" , align: "center" },
                { name: "createdAt", type: "text", validate: "required" , align: "center" }
            ]
        });

      }

      i = i + 1;

    }
  });

}


function loadorderdata(){

  var orders = [];

  var i = 0

  $.get("http://localhost:4000/order/getallOrders?limit=0&skip=0", function( data ) {
    for (var item in data) {
      orders.push({
        "orderID" : data[item]['orderID'],
        "orderDate" : data[item]['orderDate'],
        "mm8" : data[item]['mm8'],
        "mm10" : data[item]['mm10'],
        "mm12" : data[item]['mm12'],
        "mm16" : data[item]['mm16'],
        "mm20" : data[item]['mm20'],
        "mm25" : data[item]['mm25'],
        "mm32" : data[item]['mm32'],
        "bw" : data[item]['bw'],
        "userID" : data[item]['userID'],
        "totalPrice" : data[item]['totalPrice'],
        "status" : data[item]['status'],
        "cgst" : data[item]['cgst'],
        "sgst" : data[item]['sgst'],
        "igst" : data[item]['igst'],
        "ordertype" : data[item]['ordertype'],
        "deliveryStateID" : data[item]['deliveryStateID'],
        "address" : data[item]['address'],
        "createdAt" : data[item]['createdAt'],
        "utrNo" : data[item]['paymentDetails']['utrNo']


      });


      if (i == data.length -1){

        $("#jsGrid_order").jsGrid({
            width: "100%",
            height: "400px",
            data: orders,
            sorting: true,
            paging: true,

            fields: [
                { name: "orderID", type: "text",validate: "required" , align: "center" },
                { name: "orderDate", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "mm8", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "mm10", type: "text",width: 200 , validate: "required", align: "center"  },
                { name: "mm12", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "mm16", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "mm20", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "mm25", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "mm32", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "bw", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "userID", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "totalPrice", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "status", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "cgst", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "sgst", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "igst", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "ordertype", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "deliveryStateID", type: "text",width: 200 , validate: "required" , align: "center" } ,
                { name: "address", type: "text", width: 200 ,validate: "required" , align: "center" } ,
                { name: "createdAt", type: "text", width: 200 ,validate: "required" , align: "center" } ,
                { name: "utrNo", type: "text", width: 200 ,validate: "required" , align: "center" }
            ]
        });

      }

      i = i + 1;

    }
  });

}



function loadprices(){

  var prices = [];

  var i = 0

  $.get("http://localhost:4000/rates/getallrates_admin", function( data ) {
    for (var item in data) {
      prices.push({
        "distributorID" : data[item]['distributorID'],
        "product" : data[item]['product'],
        "type" : data[item]['type'],
        "rate" : data[item]['rate'],
        "restrictType" : data[item]['restrictType'],
        "modifiedAt" : data[item]['modifiedAt']


      });


      if (i == data.length -1){

        $("#jsGrid_price").jsGrid({
            width: "100%",
            height: "400px",
            data: prices,
            sorting: true,
            paging: true,

            fields: [
                { name: "distributorID", type: "text",validate: "required" , align: "center" },
                { name: "product", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "type", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "rate", type: "text",width: 200 , validate: "required", align: "center"  },
                { name: "restrictType", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "modifiedAt", type: "text", width: 200 ,validate: "required" , align: "center" }

            ]
        });

      }

      i = i + 1;

    }
  });

}




function loadfeedback(){

  var feedback = [];

  var i = 0

  $.get("http://localhost:4000/rates/getallrates_admin", function( data ) {
    for (var item in data) {
      feedback.push({
        "name" : data[item]['name'],
        "email" : data[item]['email'],
        "mobile" : data[item]['mobile'],
        "comments" : data[item]['comments'],
        "createdAt" : data[item]['createdAt']
      });

      if (i == data.length -1){

        $("#jsGrid_feedback").jsGrid({
            width: "100%",
            height: "400px",
            data: feedback,
            sorting: true,
            paging: true,

            fields: [
                { name: "name", type: "text",validate: "required" , align: "center" },
                { name: "email", type: "text",width: 200 , validate: "required" , align: "center" },
                { name: "mobile", type: "text", width: 200 ,validate: "required" , align: "center" },
                { name: "comments", type: "text",width: 200 , validate: "required", align: "center"  },
                { name: "createdAt", type: "text", width: 200 ,validate: "required" , align: "center" }
            ]
        });
      }
      i = i + 1;
    }
  });

}


function saveuser(){

  if ($("#txt_fname").val() == ""){
    alert("First Name cannot be empty")
  }
  else if ($("#txt_lname").val() == ""){
    alert("Last Name cannot be empty")
  }
  else if ($("#txt_email").val() == ""){
    alert("First Name cannot be empty")
  }
  else if ($("#txt_pwd").val() == ""){
    alert("Password cannot be empty")
  }
  else if ($("#txt_mobile").val() == ""){
    alert("Mobile cannot be empty")
  }
  else if ($("#txt_firmname").val() == ""){
    alert("Firm Name cannot be empty")
  }
  else if ($("#txt_dob").val() == ""){
    alert("Date of Birth cannot be empty")
  }
  else if ($("#txt_addr1").val() == ""){
    alert("Address1 cannot be empty")
  }
  else if ($("#txt_addr2").val() == ""){
    alert("Address2 cannot be empty")
  }
  else if ($("#txt_addr3").val() == ""){
    alert("Address3 cannot be empty")
  }
  else if ($("#txt_pincode").val() == ""){
    alert("Pincode cannot be empty")
  }
  else if ($("#txt_stateid").val() == ""){
    alert("StateID cannot be empty")
  }
  else if ($("#txt_gstno").val() == ""){
    alert("GST NO cannot be empty")
  }
  else if ($("#txt_usertype").val() == ""){
    alert("UserType cannot be empty")
  }
  else if ($("#txt_distrid").val() == ""){
    alert("DistributorID cannot be empty")
  }
  else{

    var params = {
      fName:$("#txt_fname").val(),
      lName:$("#txt_lname").val(),
      email:$("#txt_email").val(),
      saltPassword:$("#txt_pwd").val(),
      mobile:$("#txt_mobile").val(),
      firmName:$("#txt_firmname").val(),
      dateofBirth:$("#txt_dob").val(),
      address1:$("#txt_addr1").val(),
      address2:$("#txt_addr2").val(),
      address3:$("#txt_addr3").val(),
      pincode:$("#txt_pincode").val(),
      stateID:$("#txt_stateid").val(),
      gstNo:$("#txt_gstno").val(),
      userType:$("#txt_usertype").val(),
      distributorID:$("#txt_distrid").val()
    }

    $.ajax({
        url: "http://localhost:4000/user/register",
        type: "post",
        data: params,
        crossDomain : true,
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'json',
        success: function(data) {
            alert("User Registered successfully");
        },
        error: function(data) {
            alert("User Registered successfully");
        }
    }).done(function(msg) {
    });

  }


}


function saveprice(){

  if ($("#txt_rate").val() == ""){
    alert("Rate cannot be empty")
  }
  else if ($("#txt_product").val() == ""){
    alert("Product cannot be empty")
  }
  else if ($("#txt_type").val() == ""){
    alert("Type cannot be empty")
  }
  else if ($("#txt_distr").val() == ""){
    alert("Distributor cannot be empty")
  }
  else if ($("#txt_restrictType").val() == ""){
    alert("Restrict Type cannot be empty")
  }
  else{

    var params = {
      type:$("#txt_type").val(),
      distributorID:$("#txt_distr").val(),
      rate:$("#txt_rate").val(),
      product:$("#txt_product").val(),
      restrictType:$("#txt_restrictType").val()
    }


    $.ajax({
        url: "http://localhost:4000/rates/saverate",
        type: "get",
        data: params,
        crossDomain : true,
        headers: {
        },
        dataType: 'json',
        success: function(data) {

        },
        error: function(data) {
            alert("Rate updated successfully");
        }
    }).done(function(msg) {

    });

  }

}
//
// function updateorder(){
//
//   if ($("#txt_order_id").val() == ""){
//     alert("OrderID cannot be empty")
//   }
//   else if ($("#txt_order_status").val() == ""){
//     alert("Order Status cannot be empty")
//   }
//   else{
//
//     var params = {
//       orderID:$("#txt_order_id").val(),
//       status:$("#txt_order_status").val()
//     }
//
//
//     $.ajax({
//         url: "http://localhost:4000/order/updateUTR",
//         type: "post",
//         data: params,
//         crossDomain : true,
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         dataType: 'json',
//         success: function(data) {
//             alert("success");
//         }
//     }).done(function(msg) {
//         alert('done');
//     });
//
//   }
//
// }
